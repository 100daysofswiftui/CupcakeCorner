# CupcakeCorner

CupcakeCorner is a multi-screen app for ordering cupcakes

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/cupcake-corner-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- URLSession
- disabled()
- Haptic effects
- Encoding
- \@Observable classes
- URLSession